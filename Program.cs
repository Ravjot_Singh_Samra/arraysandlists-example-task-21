﻿using System;
using System.Collections.Generic;

namespace arraysandlists_example_task_21
{
    class Program
    {
        static void Main(string[] args)
        {
            var movies = new string[5] {"Movie 1", "Movie 2", "Movie 3", "Movie 4", "Movie 5"};
            
            movies[0] = "New Movie 1";
            movies[2] = "New Movie 3";

            Array.Sort(movies);

            int moviesLength = movies.Length;
            Console.WriteLine($"The array's length is {moviesLength}.");
            Console.WriteLine(string.Join("\n", movies));
        

            var goodPlacesToEat = new List<string> {"Shop 1", "Shop 2", "Shop 3", "Shop 4", "Shop 5"};
            Console.WriteLine($"\nThe list's length is {goodPlacesToEat.Count}."); // this is a better way than line 17 and 18
        
            goodPlacesToEat.Remove("Shop 3");
            Console.WriteLine(string.Join("\n", goodPlacesToEat));
        }
    }
}
